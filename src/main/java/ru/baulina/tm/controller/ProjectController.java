package ru.baulina.tm.controller;

import ru.baulina.tm.api.controller.IProjectController;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.model.Project;
import ru.baulina.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = projectService.findAll();
        for (Project project: projects) System.out.println(project);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void createProjects() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final  String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final  String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
