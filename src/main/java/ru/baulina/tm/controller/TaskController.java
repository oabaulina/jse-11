package ru.baulina.tm.controller;

import ru.baulina.tm.api.controller.ITaskController;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.model.Task;
import ru.baulina.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        for (Task task: tasks) System.out.println(task);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void createTasks() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final  String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final  String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
