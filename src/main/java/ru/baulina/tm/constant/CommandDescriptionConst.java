package ru.baulina.tm.constant;

public class CommandDescriptionConst {

    public static final String HELP = "Display list of terminal commands.";

    public static final String VERSION = "Display program version.";

    public static final String ABOUT = "Display developer info.";

    public static final String EXIT = "Close application.";

    public static final String INFO = "Display information about system.";

    public static final String ARGUMENTS = "Show program arguments.";

    public static final String COMMANDS = "Show program commands.";

    public static final String TASK_CREATE = "Create new task.";

    public static final String TASK_LIST = "Show task list.";

    public static final String TASK_CLEAR = "Remove all tasks.";

    public static final String PROJECT_CREATE = "Create new project.";

    public static final String PROJECT_LIST = "Show task project.";

    public static final String PROJECT_CLEAR = "Remove all projects.";

}
