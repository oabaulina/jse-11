package ru.baulina.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProjects();

}
