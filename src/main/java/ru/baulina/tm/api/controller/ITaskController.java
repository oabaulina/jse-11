package ru.baulina.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTasks();

}
